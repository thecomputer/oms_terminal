public class Command
{
    private Command next;
    private String command;
    public Command(String info)
    {
        this.command = info;
        this.next = null;
    }
    /*הפעולה בונה ומחזירה חוליה, שהערך שלה הוא info
      והחוליה העוקבת לה היא החוליה next */
    public Command(String info, Command next)
    {
        this.command = info;
        this.next = next;
    }
    /* הפעולה מחזירה את הערך של החוליה הנוכחית **/
    public String GetInfo()
    {
        return command;
    }
    /* הפעולה מחזירה את החוליה העוקבת לחוליה הנוכחית **/
    public Command GetNext()
    {
        return next;
    }
    /* הפעולה קובעת את ערך החוליה הנוכחית להיות  info **/
    public void SetInfo(String info)
    {
        this.command = info;
    }
    /* הפעולה קובעת את החוליה העוקבת לחוליה הנוכחית להיות החוליה next **/
    public void SetNext(Command next)
    {
        this.next = next;
    }
    /* הפעולה מחזירה מחרוזת המתארת את החוליה הנוכחית */


    public  String ToString()
    {
        return this.command.toString();
    }
}//Class