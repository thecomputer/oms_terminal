package SQL;

public class SQLParser {


	public static final String CREATE_TABLE_USERS="CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT,username VARCHAR(255) NOT NULL,"+
			"hashed_password VARCHAR(255),salt VARCHAR(255),email VARCHAR(255),permission_role INT,token VARCHAR(255),CONSTRAINT users_pk PRIMARY KEY (id));";
	public static final String CREATE_TABLE_NOTES="CREATE TABLE notes (id INT NOT NULL AUTO_INCREMENT,userid INT,title VARCHAR(255) NOT NULL,"+
			"description TEXT,unixtimestamp BIGINT, CONSTRAINT notes_pk PRIMARY KEY (id));";
	public static final String CREATE_TABLE_TIMETABLE="CREATE TABLE timetable (id INT NOT NULL AUTO_INCREMENT,userid INT,title VARCHAR(255) NOT NULL,description TEXT,"+
				"notes TEXT,day TINYINT,hour TINYINT,minute TINYINT,CONSTRAINT timetable_pk PRIMARY KEY (id));";
	public static final String CREATE_TABLE_CALENDAR="CREATE TABLE calendar (id INT NOT NULL AUTO_INCREMENT,userid INT,title VARCHAR(255) NOT NULL,description TEXT,"+
					"notes TEXT,unixtimestamp BIGINT,CONSTRAINT calendar_pk PRIMARY KEY (id));";
	
	
	public static final String DELETE_TABLE_USERS="DROP Table IF EXISTS users;";
	public static final String DELETE_TABLE_NOTES="DROP Table IF EXISTS notes;";
	public static final String DELETE_TABLE_TIMETABLE="DROP Table IF EXISTS timetable;";
	public static final String DELETE_TABLE_CALENDAR="DROP Table IF EXISTS calendar;";
	
	public static final String LIST_USERS="SELECT id,username,email,permission_role FROM users WHERE 1=1;";
	public static final String ADD_USER="INSERT INTO users VALUES(NULL,?,?,?,?,3,'');";
	public static final String SELECT_USER="SELECT id FROM users WHERE username=?";
	public static final String DEL_USER="DELETE FROM users WHERE id=?";
	
	public static final String MOD_USER_PASS="UPDATE users SET hashed_password=? ,salt=? WHERE id=?";
	public static final String MOD_USER_EMAIL="UPDATE users SET email=? WHERE id=?";
	public static final String MOD_USER_USERNAME="UPDATE users SET username=? WHERE id=?";

	
}
