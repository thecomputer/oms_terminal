
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;

import models.SQLConnection;

public class Program {

	private static final String filename = "/etc/oms/hashes";
	public static ArrayList<SQLConnection> records;	
	public static SQLConnection current_hash;
	public static Connection con;
	private static void connectHashFile()
	{
	  records = new ArrayList<SQLConnection>();
	  try
	  {
	    BufferedReader reader = new BufferedReader(new FileReader(filename));
	    String line;
	    while ((line = reader.readLine()) != null)
	    {
	    	String[] parts = line.split("::");
	    	records.add(new SQLConnection(parts[0],parts[1],parts[2],parts[3],parts[4],parts[5]));
	    }
	    reader.close();
	  }
	  catch (Exception e)
	  {
	    System.err.format("Failed to read '%s'.", filename);
	    e.printStackTrace();
	  }
	}
	public static void rewriteHashFile()
	{
	  //records = new ArrayList<SQLConnection>();
	  try
	  {
		  PrintWriter writer = new PrintWriter(filename, "UTF-8");
		  for(int i=0;i<records.size();i++)
		  {
			  writer.println(records.get(i).toString());
		  }
		  writer.close();
	  }
	  catch (Exception e)
	  {
	    System.err.format("Failed to read '%s'.", filename);
	    e.printStackTrace();
	  }
	}
	
	public static void main(String[] args) {
		
		/*try {
			System.out.println(PasswordStorage.verifyPassword("help","sha1:64000:18:Aysypsur4zxGkRlB8q+PpnIIAr0z6SEe:7EKAUYsn0Bss0AjqQn94+GnE"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		Program.connectHashFile();
		Scanner sc = new Scanner(System.in);
        boolean run = true;
        while(run)
        {
        	System.out.print("shell> ");
        	sc = new Scanner(System.in); 
        	String command = sc.nextLine();
        	if(command.equals("exit"))
        		run = false;
        	else
        	{
        		
        		//Controller.AddCommand(commands,command);
        		Controller.ExecuteCommand(command);
        		
        	}	
        	
        }
        sc.close();
        rewriteHashFile();

	}
	
}
