import java.sql.ResultSet;
import java.sql.Statement;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import java.sql.PreparedStatement;

import java.io.Console;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;

import SQL.SQLParser;

public class AdminCommands {

	public static void ListUsers()
	{
		try {
			if(Program.current_hash != null)
			{
				System.out.println("List of users:");
				System.out.println("ID | username | Email | Permission Ring");
				Connection con = Program.current_hash.getConnection();	
				Statement st = con.createStatement();
		        ResultSet rs = st.executeQuery(SQLParser.LIST_USERS);
				while (rs.next()) {
					System.out.println(rs.getString(1) + " | " + rs.getString(2) + " | " + rs.getString(3) + " | " + rs.getInt(4)); //gets the first column's rows.
				}
			}
			else
			{
				System.out.println("You have to rehash before using this command!");
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	public static void useradd(String[] params)
	{
		if(params != null)
		{
			if(params.length == 1)
			{
				try {
					if(Program.current_hash != null)
					{
						Connection con = Program.current_hash.getConnection();
						PreparedStatement st = (PreparedStatement) con.prepareStatement(SQLParser.SELECT_USER);
						st.setString(1, params[0]);
				        ResultSet rs = st.executeQuery();
				        int i=0;
						while (rs.next()) {
							i++;
						}
						if(i>0)
						{
							System.out.println(params[0]+ " already exists!");
						}
						else
						{
							st = (PreparedStatement) con.prepareStatement(SQLParser.ADD_USER);
							st.setString(1, params[0]);
							st.setString(2, "");
							st.setString(3, "");
							st.setString(4, "");
					        st.executeUpdate();
					        userpass(params);
						}
					}
					else
					{
						System.out.println("You have to rehash before using this command!");
					}
					
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("Correct syntax: useradd [username]");
			}
		}
		else
		{
			System.out.println("Correct syntax: useradd [username]");
		}
		
	}
	public static void userdel(String[] params)
	{
		if(params != null)
		{
			if(params.length == 1)
			{
				try {
					if(Program.current_hash != null)
					{
						
						Connection con = Program.current_hash.getConnection();
						PreparedStatement st = (PreparedStatement) con.prepareStatement(SQLParser.SELECT_USER);
						st.setString(1, params[0]);
				        ResultSet rs = st.executeQuery();
				        int i=0;
				        int id=-1;
						while (rs.next()) {
							i++;
							id=rs.getInt(1);
						}
						if(i==1)
						{
							if(id==1)
							{
								System.out.println("You can't delete the admin user!");
							}
							else
							{
								st = (PreparedStatement) con.prepareStatement(SQLParser.DEL_USER);
								st.setInt(1, id);
						        st.executeUpdate();
							}
							
						}
						else
						{
							System.out.println("user " + params[0]+ " doesn not exist!");
						}
						
					}
					else
					{
						System.out.println("You have to rehash before using this command!");
					}
					
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("Correct syntax: userpass [username]");
			}
		}
		else
		{
			System.out.println("Correct syntax: userpass [username]");
		}
	}
	public static void userpass(String[] params)
	{
		
		if(params != null)
		{
			if(params.length == 1)
			{
				try {
					if(Program.current_hash != null)
					{
						
						Connection con = Program.current_hash.getConnection();
						PreparedStatement st = (PreparedStatement) con.prepareStatement(SQLParser.SELECT_USER);
						st.setString(1, params[0]);
				        ResultSet rs = st.executeQuery();
				        int i=0;
				        int id=-1;
						while (rs.next()) {
							i++;
							id=rs.getInt(1);
						}
						if(i==1)
						{
							
							Console console = System.console();
							System.out.print("Please enter a password: ");
							char[] passwordChars = console.readPassword();
							
							String parts = PasswordStorage.createHash(passwordChars);
							// format: algorithm:iterations:hashSize:salt:hash
							String[] params_part = parts.split(":");
							
							//System.out.println(parts);
							
							st = (PreparedStatement) con.prepareStatement(SQLParser.MOD_USER_PASS);
							st.setString(1, params_part[4]);
							st.setString(2, params_part[3]);
							st.setInt(3, id);
							
					        st.executeUpdate();
						}
						else
						{
							System.out.println("user " + params[0]+ " doesn not exist!");
						}
						
					}
					else
					{
						System.out.println("You have to rehash before using this command!");
					}
					
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("Correct syntax: userpass [username]");
			}
		}
		else
		{
			System.out.println("Correct syntax: userpass [username]");
		}
	}
	
	public static void usermod(String[] params)
	{
		if(params != null)
		{
			if(params.length >= 2)
			{
				if(Program.current_hash != null)
				{
					switch(params[1])
					{
						case "password":
							if(params.length == 2)
							{
								String[] new_params = {params[0]};
								userpass(new_params);
							}
							else
							{
								System.out.println("Correct syntax: usermod [username] [settings-name] [value]");
								System.out.println("Valid setting-name strings: password,email,username");
							}
							break;
						case "email":
							if(params.length == 3)
							{
								changeEmail(params[0],params[2]);	
							}
							else
							{
								System.out.println("Correct syntax: usermod [username] [settings-name] [value]");
								System.out.println("Valid setting-name strings: password,email,username");
							}
							
							break;
						case "username":
							if(params.length == 3)
							{
								changeUsername(params[0],params[2]);	
							}
							else
							{
								System.out.println("Correct syntax: usermod [username] [settings-name] [value]");
								System.out.println("Valid setting-name strings: password,email,username");
							}
							break;
					}
				}
				else
				{
					System.out.println("You have to rehash before using this command!");
				}
			}
			else
			{
				System.out.println("Correct syntax: usermod [username] [settings-name] [value]");
				System.out.println("Valid setting-name strings: password,email,username");
			}
			
		}
		else
		{
			System.out.println("Correct syntax: usermod [username] [settings-name] [value]");
		}
	}
	private static void changeEmail(String username,String value) {
		Connection con = Program.current_hash.getConnection();
		try {
			
				
			PreparedStatement st = (PreparedStatement) con.prepareStatement(SQLParser.SELECT_USER);
				st.setString(1, username);
				ResultSet rs = st.executeQuery();
		        int i=0;
		        int id=-1;
				while (rs.next()) {
					i++;
					id=rs.getInt(1);
				}
				if(i==1)
				{
					st = (PreparedStatement) con.prepareStatement(SQLParser.MOD_USER_EMAIL);
					st.setString(1, value);
					st.setInt(2, id);
			        st.executeUpdate();
				}
				else 
				{
					System.out.println("user " + username+ " doesn not exist!");
				}
			
			
				
				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	private static void changeUsername(String username,String value) {
		
		Connection con = Program.current_hash.getConnection();
		try {
			PreparedStatement st = (PreparedStatement) con.prepareStatement(SQLParser.SELECT_USER);
			st.setString(1, value);
	        ResultSet rs = st.executeQuery();
	        int d=0;
	        int id=-1;
			while (rs.next()) {
				d++;
				id=rs.getInt(1);
			}
			if(d != 1) {
					st = (PreparedStatement) con.prepareStatement(SQLParser.SELECT_USER);
					st.setString(1, username);
					rs = st.executeQuery();
					int i=0;
					id=-1;
					while (rs.next()) {
						i++;
						id=rs.getInt(1);
					}
				if(i==1)
				{
					st = (PreparedStatement) con.prepareStatement(SQLParser.MOD_USER_USERNAME);
					st.setString(1, value);
					st.setInt(2, id);
					st.executeUpdate();
				}
				else
				{
				System.out.println("user " + username+ " doesn not exist!");
				}
			}
			else
			{
				System.out.println("user " + value + " already exist!");
			}
				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
