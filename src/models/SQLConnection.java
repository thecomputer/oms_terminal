package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class SQLConnection {

	private String password;
	private String address;
	private String port;
	private String user;
	private String database;
	private String hash;	
	
	public SQLConnection(String hash,String addr,String port,String user,String pass,String db)
	{
		this.hash =hash;
		this.address=addr;
		this.user =user;
		this.password= pass;
		this.port=port;
		this.database=db;
	}
	
	public String toString()
	{
		return this.getHash()+"::"+
				this.getAddress()+"::"+this.getPort()+"::"+this.getUsername()+
				"::"+this.getPassword()+"::"+this.getDatabase();
		
	}
	public String toSecureString()
	{
		char[] emptyString = new char[this.getPassword().length()];
		for(int i=0;i<this.getPassword().length();i++)
		{
			emptyString[i] = ' ';
		}
		String securePassword = new String(emptyString);
		
		return this.getHash()+" | "+
				this.getAddress()+" | "+this.getPort()+" | "+this.getUsername()+
				" | "+securePassword+" | "+this.getDatabase();
		
	}
	
	public Connection getConnection()
	{
		//String url = "jdbc:mysql://localhost:3306/testdb?useSSL=false&autoReconnect=true";
		String url = "jdbc:mysql://"+this.getAddress()+":"+this.getPort()+"/"+this.getDatabase()+"?useSSL=false&autoReconnect=true&useUnicode=true&characterEncoding=utf-8";

        String user = this.getUsername();
        String password = this.getPassword();
        
        	
        	try {
        		Connection con = DriverManager.getConnection(url, user, password);
        		return con;
        		
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
		return null;
	}
	
	public String getPassword()
	{
		return this.password;
	}
	public String getUsername()
	{
		return this.user;
	}
	public String getAddress()
	{
		return this.address;
	}
	public String getPort()
	{
		return this.port;
	}
	public String getDatabase()
	{
		return this.database;
	}
	
	public String getHash()
	{
		return this.hash;
	}
	
	
}
