
public class Commands {
		
	public static void ClearConsole()
	{
		for (int i = 0; i < 48; ++i) System.out.println();
	}
	
	public static void Help()
	{
		System.out.println("List of commands:");
		System.out.println("connect [host] [port] [user] [database] – Define a connection to MySQL server\n" + 
				"deconnect [id] – Delete a connection to MySQL Server\n" + 
				"lshash – Show available connections\n" + 
				"rehash [id] – Connect/Reconnect to a MySQL Server\n" + 
				"install – Install tables\n" + 
				"deinstall – Remove tables and installation data\n" + 
				"lsusers – Show users\n" + 
				"useradd [username] – Add a new user\n" + 
				"userdel [username] – Delete a user \n" + 
				"userpass [username] – Change password for user\n" + 
				"usermod [username] [settings-name] [value] – Modify a user. Valid settings-name: email,password,username\n" +  
				"lsabout – About this software\n" + 
				"lsversion – Show version");
	}
	
	public static void About()
	{
		System.out.println("Copyright 2018-2019 Liav Albani\n" + 
				"\n" + 
				"Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"),\n"
				+ "to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,\n"
				+ "and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n" + 
				"\n" + 
				"The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n" + 
				"\n" + 
				"THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n"
				+ "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,\n"
				+ "WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.");
	}
	
	public static void ShowVersion()
	{
		System.out.println("OMS Console Version 1.0");
	}
	
}
