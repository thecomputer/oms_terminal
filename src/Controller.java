public class Controller {

	private static void Commands(String command, String[] params)
	{
		switch(command)
		{
			case "clear":
				Commands.ClearConsole();
				break;
			case "?":
				Commands.Help();
				break;
			case "help":
				Commands.Help();
				break;
			case "lshash":
				ServerCommands.showhashes();
				break;
			case "rehash":
				ServerCommands.rehash(params);
				break;
			case "connect":
				ServerCommands.connect(params);
				break;
			case "deconnect":
				ServerCommands.deconnect(params);
				break;
			case "install":
				ServerCommands.install(params);
				break;
			case "deinstall":
				ServerCommands.deinstall();
				break;
			case "lsusers":
				AdminCommands.ListUsers();
				break;
			case "useradd":
				AdminCommands.useradd(params);
				break;
			case "userpass":
				AdminCommands.userpass(params);
				break;
			case "usermod":
				AdminCommands.usermod(params);
				break;
			case "userdel":
				AdminCommands.userdel(params);
				break;
			case "lsabout":
				Commands.About();
				break;
			case "lsversion":
				Commands.ShowVersion();
				break;
			default:
				Error(command);
				break;
		}
	}
	
	private static void Error(String command)
	{
		if(command != null)
		{
			System.out.println("ERROR: "+command + " not found!");

		}
	}
	
	public static void ExecuteCommand(String command)
	{
		String[] parts = command.split(" ");
		if(parts[0].equals(""))
		{
			Error(null);
		}
		else
		{
			String command_exec = parts[0]; 
			String[] params = getParams(parts);
		
			Commands(command_exec,params);
		
		}
		
	}
	
	private static String[] getParams(String[] command)
	{
		if(command == null)
			return null;
		
		String[] params = new String[command.length-1];
		if(command.length == 1)
			return null;
		
		if(command.length == 2)
		{
			params[0]=command[1];
			return params;
		}
		
		for(int i=0; i<command.length-1;i++)
			params[i] = command[i+1];
		return params;
	}
	
	/*private static void AddCommand(Command linkedList, String new_command) {
		Command list = linkedList;
		while(list.GetNext() != null)
		{
			list = list.GetNext();
		}
		list.SetNext(new Command(new_command,null));
	}
	private static void ShowHistory(Command commands)
	{
		int i = 0;
		while(commands != null)
		{
			System.out.println("["+i+"]: " + commands.GetInfo());
			commands = commands.GetNext();
			i++;
		}
	}
	
	/*public final static void clearConsole()
	{
	    try
	    {
	        final String os = System.getProperty("os.name");

	        if (os.contains("Windows"))
	        {
	            Runtime.getRuntime().exec("cls");
	        }
	        else
	        {
	            Runtime.getRuntime().exec("clear");
	        }
	    }
	    catch (final Exception e)
	    {
	    	Logger lgr = Logger.getLogger(Program.class.getName());
	    	lgr.log(Level.SEVERE, e.getMessage(), e);
	    }
	}*/
	
}
