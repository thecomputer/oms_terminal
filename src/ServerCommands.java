import java.io.Console;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Scanner;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import SQL.SQLParser;
import models.RandomString;
import models.SQLConnection;

public class ServerCommands {

	public static void rehash(String[] params)
	{
		if(params == null)
		{
			System.out.println("Correct syntax: rehash [token]");
			System.out.println("ERROR: Please specify a token!");
			showhashes();
		}
		else
		{
			if(params.length==1)
			{
				try {
					Program.current_hash = Program.records.get(Integer.parseInt(params[0])-1);
					Program.con = Program.current_hash.getConnection();
					//login();

				}
				catch(Exception e)
				{
					System.out.println("Correct syntax: rehash [token]");
					System.out.println("ERROR: Please specify a token!");
					showhashes();
				}
			}
			else
			{
				System.out.println("Correct syntax: rehash [token]");
				System.out.println("ERROR: Please specify a token!");
				showhashes();
			}
		}
	}
	
	public static void showhashes()
	{

			System.out.println("List of all tokens available:");
			System.out.println("ID | Hash | Address | Port | Username | Password | Database");
			for(int i=0;i<Program.records.size();i++)
			{
				System.out.print((i+1)+ " | ");
				System.out.println(Program.records.get(i).toSecureString());
			}
	}
	
	public static void deinstall()
	{

		if(Program.current_hash != null)
		{
					
			Scanner sc = new Scanner(System.in);					
			System.out.print("WARNING! DO NOT IGNORE!: Proceeding with the process may destroy all of your"
					+" previous OMS data in the database. Are you sure you want to continue? [y/N] ");
			String input = sc.nextLine();
			if(input.equals("y") || input.equals("Y"))
			{
				
                try {

                    //	Program.con.
                	Connection con = Program.current_hash.getConnection();
                	Statement st = con.createStatement();
                	st.executeUpdate(SQLParser.DELETE_TABLE_CALENDAR);
                	st.executeUpdate(SQLParser.DELETE_TABLE_NOTES);
                	st.executeUpdate(SQLParser.DELETE_TABLE_USERS);
                	st.executeUpdate(SQLParser.DELETE_TABLE_TIMETABLE);
                	st.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//sc.close();
		
		}
		else
		{
			System.out.println("You have to rehash before using this command!");	
		}
			
	}
	
	public static void install(String[] params)
	{
		if(params != null)
		{
			if(params.length == 1)
			{
				if(Program.current_hash != null)
				{
					Scanner sc = new Scanner(System.in);					
					System.out.print("WARNING! DO NOT IGNORE!: Proceeding with the process may destroy all of your"
							+" previous OMS data in the database. Are you sure you want to continue? [y/N] ");
					String input = sc.nextLine();
					if(input.equals("y") || input.equals("Y"))
					{
						//sc.close();
						SecureRandom random = new SecureRandom();
						byte[] salt = new byte[16];
						random.nextBytes(salt);
						String pass = "admin";
						KeySpec spec = new PBEKeySpec(pass.toCharArray(), salt, 65536, 128);
						byte[] hash = null;
						try {
							SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
							try {
								hash = factory.generateSecret(spec).getEncoded();
							} catch (InvalidKeySpecException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (NoSuchAlgorithmException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
						if(params[0].equals("default"))
						{
							try {
								
			                    //	Program.con.
			                	Connection con = Program.current_hash.getConnection();
			                	Statement st = con.createStatement();
			                	st.executeUpdate(SQLParser.CREATE_TABLE_NOTES);
			                	st.executeUpdate(SQLParser.CREATE_TABLE_USERS);
			                	st.executeUpdate(SQLParser.CREATE_TABLE_TIMETABLE);
			                	st.executeUpdate(SQLParser.DELETE_TABLE_CALENDAR);
			                	
			                	st.executeUpdate("INSERT INTO users VALUES(NULL,'admin','"+hash+"','"+salt+"','',0,'');");
			                	st.close();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						else
						{
							if(params[0].equals("expert"))
							{
								try {
									System.out.print("Enter a username:");
									sc = new Scanner(System.in);
									input = sc.nextLine();
									
				                    //	Program.con.
				                	Connection con = Program.current_hash.getConnection();
				                	Statement st = con.createStatement();
				                	st.executeUpdate(SQLParser.CREATE_TABLE_NOTES);
				                	st.executeUpdate(SQLParser.CREATE_TABLE_USERS);
				                	st.executeUpdate(SQLParser.CREATE_TABLE_TIMETABLE);
				                	st.executeUpdate(SQLParser.DELETE_TABLE_CALENDAR);
				                	
				                	st.executeUpdate("INSERT INTO users VALUES(NULL,'admin','"+hash+"','"+salt+"','',0,'');");
				                	st.close();
				                	
				                	String[] params_new = {"admin","username",input};
				                	AdminCommands.usermod(params_new);
				                	String[] params_new2 = {input,"password"};
				                	AdminCommands.usermod(params_new2);
				                	
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else
							{
								System.out.println("Correct syntax: install [install-type]");
								System.out.println("install-type can be 'default' or 'expert'");
								System.out.println("ERROR: Please specify the correct argument!");
							}
						}
					}
					
				}
				else
				{
					System.out.println("You have to rehash before using this command!");	
				}
				
			}
			else
			{
				System.out.println("Correct syntax: install [install-type]");
				System.out.println("install-type can be 'default' or 'expert'");
				System.out.println("ERROR: Please specify the correct argument!");
			}
		}
		else
		{
			System.out.println("Correct syntax: install [install-type]");
			System.out.println("install-type can be 'default' or 'expert'");
			System.out.println("ERROR: Please specify the correct argument!");
		}
	}
	
	public static void deconnect(String[] params)
	{

		if(params != null)
		{
			if(params.length == 1)
			{
				if(Program.current_hash != null)
				{
					try {
			        	if(Program.current_hash.getConnection() != null)
			        		Program.current_hash.getConnection().close();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		        
				Program.current_hash = null;
				
				
				try {
					Program.records.remove(Integer.parseInt(params[0])-1);

				}
				catch(Exception e)
				{
					System.out.println("Correct syntax: deconnect [token]");
					System.out.println("ERROR: Please specify the correct argument!");
				}
				Program.rewriteHashFile();
			}
			else
			{
				System.out.println("Correct syntax: deconnect [token]");
				System.out.println("ERROR: Please specify the correct argument!");
			}
		}
		else
		{
			System.out.println("Correct syntax: deconnect [token]");
			System.out.println("ERROR: Please specify the correct argument!");
		}
	}
	public static void connect(String[] params)
	{

		if(params != null)
		{
			if(params.length == 4)
			{
		        Console console = System.console();
				System.out.print("Please enter a password: ");
				char[] passwordChars = console.readPassword();
				String password = new String(passwordChars);
				
				RandomString gen = new RandomString(8);
				
				String hashRandom = gen.nextString();
				Program.records.add(new SQLConnection(hashRandom,params[0],params[1],params[2],password,params[3]));
				Program.rewriteHashFile();
			}
			else
			{
				System.out.println("Correct syntax: connect [host] [port] [user] [database]");
				System.out.println("ERROR: Please specify the correct arguments!");
			}
		}
		else
		{
			System.out.println("Correct syntax: connect [host] [port] [user] [database]");
			System.out.println("ERROR: Please specify the correct arguments!");
		}
	}
	/*
	private static void login()
	{
		if(Program.current_hash == null)
		{
			System.out.println("ERROR! Please rehash with the appropriate session with: rehash [token]");
		}
		else
		{
			//String url = "jdbc:mysql://localhost:3306/testdb?useSSL=false";
			String url = "jdbc:mysql://"+Program.current_hash.getAddress()+":"+Program.current_hash.getPort()+"/"+Program.current_hash.getDatabase()+"?useSSL=false";

	        String user = Program.current_hash.getUsername();
	        String password = Program.current_hash.getPassword();
	        
	        String query = "SELECT VERSION()";
	        	
	        	try (Connection con = DriverManager.getConnection(url, user, password);
	                    Statement st = con.createStatement();
	                    ResultSet rs = st.executeQuery(query)) {

	                    if (rs.next()) {
	                        
	                        System.out.println("Database powered by " +  rs.getString(1));
	                    }
	                    //Program.current_hash.setConnection(con);
	                    System.out.println("Now connected.");
	                                      

	                } catch (SQLException ex) {
	                    System.out.println("ERROR Complete Login");
	                    Logger lgr = Logger.getLogger(Program.class.getName());
	                    lgr.log(Level.SEVERE, ex.getMessage(), ex);
	                }
		}*/
		 
        	
        
}
	
